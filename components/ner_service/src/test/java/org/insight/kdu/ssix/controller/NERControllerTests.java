package org.insight.kdu.ssix.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class NERControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void noParamShouldReturnError() throws Exception {
        this.mockMvc.perform(get("/"))
                .andExpect(status().is(400));
    }

    @Test
    public void paramTextsShouldReturnCompany() throws Exception {

        this.mockMvc.perform(get("/")
                .param("text", "Apple Debt: Cheap Compared To Peers http://t.co/zgds6dBm1 $AAPL"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$..[?(@.type=='Company')].features.companyname")
                        .value("Apple"));
    }

}