package org.insight.kdu.ssix.controller;

/**
 * Created by waqas on 4/7/17.
 */
public class AnnotationRequest {


    private String text;
    private String mimeType;

    public void setText(String text) { this.text = text; }
    public String getText() { return text; }

    public void setMimeType(String mimeType) { this.mimeType = mimeType; }
    public String getMimeType() {
        // treat empty as null
        return ("".equals(mimeType) ? null : mimeType);
    }
}
