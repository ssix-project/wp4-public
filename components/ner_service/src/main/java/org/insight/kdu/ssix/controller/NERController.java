package org.insight.kdu.ssix.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import gate.*;
import gate.util.GateException;
import org.insight.kdu.ssix.service.NERService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * Created by waqas on 3/22/17.
 */

@RestController
public class NERController {

    private static final Logger log = LoggerFactory.getLogger(NERController.class);

    public static final String FEATURE_ENTITY_ID = "entity_id";
    public static final String FEATURE_ENTITY_NAME = "entity_name";
    public static final String FEATURE_ENTITY_TYPE = "entity_type";
    public static final String FEATURE_DOMAIN = "domain";
    public static final String FEATURE_COUNTRY = "country";
    public static final String FEATURE_PARENT_ID = "parent_id";
    public static final String LABEL = "label";

    private static final Collection<String> ANNOTATION_TYPES = Arrays.
            asList("Company", "Product", "Person","Stock","ETF","Future","Forex", "Index");

    @Autowired
    private NERService nerService;

    @ModelAttribute("allMimeTypes")
    public Set<String> registeredMimeTypes() {
        return DocumentFormat.getSupportedMimeTypes();
    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    public ResponseEntity process(
            @ModelAttribute("params") AnnotationRequest params,
            BindingResult result,
            Map<String, Object> model) throws GateException {

        final ObjectMapper mapper = new ObjectMapper();
        final ObjectNode restResult = mapper.createObjectNode();
        final Map<String, ObjectNode> targetMap = new HashMap<>();

        model.put("params", params);
        if(result.hasErrors()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } else if (params.getText()== null || params.getText().equals("")){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("param text is expected");
        } else {
            Document doc = null ;
            try {
                log.debug("Creating document");
                doc = (Document) Factory.createResource("gate.corpora.DocumentImpl",
                        Utils.featureMap("stringContent", params.getText(),
                                "mimeType", params.getMimeType()));

                final Document populatedDoc = nerService.processWithGate(doc);

                final AnnotationSet annotations = populatedDoc.getAnnotations();
                annotations.stream()
                        .filter( annotation -> ANNOTATION_TYPES.contains(annotation.getType()))
                        .forEach( annotation -> {
                            final String label = Utils.stringFor(populatedDoc, annotation);
                            final ObjectNode target = Objects.nonNull(targetMap.get(label))?
                                    targetMap.get(label) : mapper.createObjectNode();

                            final ObjectNode targetNode = populateTarget(annotation, label, target);
                            targetNode.putArray("annotations").add(getTextAnnotationObject(annotation,mapper));
                            targetMap.put(label, targetNode);
                            log.info("Recognized entity id {} with label {}.",target.get(FEATURE_ENTITY_ID),label);
                        });

            } finally {
                Factory.deleteResource(doc);
            }
        }

        restResult.set("targets", mapper.convertValue(targetMap, JsonNode.class));


        try {
            final String response = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(restResult);
            return ResponseEntity.ok(response);
        } catch (JsonProcessingException e) {
            log.error("ERROR while mapping result json to response: {}", e.getMessage(), e);
            return ResponseEntity.ok("");
        }
    }

    private ObjectNode populateTarget(Annotation annotation, String label, ObjectNode target) {
        final FeatureMap annotationFeatures = annotation.getFeatures();

        final String id = (String) annotationFeatures.get(FEATURE_ENTITY_ID);
        if (Objects.nonNull(id)) { target.put(FEATURE_ENTITY_ID, id); }

        final String name = (String) annotationFeatures.get(FEATURE_ENTITY_NAME);
        if (Objects.nonNull(name)) { target.put(FEATURE_ENTITY_NAME, name); }

        final String parentId = (String) annotationFeatures.get(FEATURE_PARENT_ID);
        if (Objects.nonNull(parentId)) { target.put(FEATURE_PARENT_ID, parentId); }

        final String type = (String) annotationFeatures.get(FEATURE_ENTITY_TYPE);
        if (Objects.nonNull(type)) { target.put(FEATURE_ENTITY_TYPE, type); }

        final String topic = (String) annotationFeatures.get(FEATURE_DOMAIN);
        if (Objects.nonNull(topic)) { target.put(FEATURE_DOMAIN, topic); }

        /*final String country = (String) annotationFeatures.get(FEATURE_COUNTRY);
        if (Objects.nonNull(country)) { target.put(FEATURE_COUNTRY, country); }*/


        //target.put("WP3_search", (String)annotationFeatures.get("WP3_search"));
        //target.put("exchange", (String)annotationFeatures.get("exchange"));
        //target.put("company_sector", (String)annotationFeatures.get("company_sector"));
        //target.put("company_industry", (String)annotationFeatures.get("company_industry"));
        //target.put("index_component", (String)annotationFeatures.get("index_component"));
        //target.put("DUPLICATE_TICKER", (String)annotationFeatures.get("DUPLICATE_TICKER"));

        return target;
    }

    private ObjectNode getTextAnnotationObject(Annotation annotation, ObjectMapper mapper) {

        final ObjectNode textAnnotation = mapper.createObjectNode();
        textAnnotation.put("start", annotation.getStartNode().getOffset());
        textAnnotation.put("end",annotation.getEndNode().getOffset());
        textAnnotation.put("type", annotation.getType());

        return textAnnotation;
    }

}