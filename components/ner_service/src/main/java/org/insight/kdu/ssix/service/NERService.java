package org.insight.kdu.ssix.service;


import gate.*;
import gate.util.GateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by waqas on 3/16/17.
 */
public class NERService{

    private static final Logger log = LoggerFactory.getLogger(NERService.class);

    private static AtomicInteger indexCounter = new AtomicInteger(0);

    private int myIndex = indexCounter.getAndIncrement();

    private CorpusController gateApplication;
    private Corpus corpus;


    @Autowired
    public NERService(CorpusController gateApplication){
        this.gateApplication = gateApplication;
    }

    // called after dependencies have been injected, but before processWithGate
    // can be called
    @PostConstruct
    public void init() throws GateException {
        corpus = Factory.newCorpus("Service  corpus");
        gateApplication.setCorpus(corpus);
    }

    // called at application shutdown time
    @PreDestroy
    public void destroy() {
        Factory.deleteResource(corpus);
        Factory.deleteResource(gateApplication);
    }

    public Document processWithGate(Document doc) throws GateException {
        try {
            log.info("Processing {} characters with handler {}", doc.getContent().size(), myIndex);
            corpus.add(doc);
            // run the pipeline
            gateApplication.execute();
            // store the handler ID for display in the results table
            doc.getFeatures().put("handledBy", myIndex);
            return doc;

        } finally {
            corpus.clear();
        }
    }
}
