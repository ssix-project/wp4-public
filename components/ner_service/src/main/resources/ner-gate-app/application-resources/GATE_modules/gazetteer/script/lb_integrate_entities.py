# The ssix database first needs to be saved into a csv file
import csv
from pprint import pprint

# Write the csv file into a txt file, with | as the delimiter between columns. The results are saved into a txt file called "fout.txt".
with open('../script/whole_data.csv', 'r', encoding='mac_roman') as fin, \
     open('fout.txt', 'w') as fout:
    reader = csv.DictReader(fin)
    writer = csv.DictWriter(fout, reader.fieldnames, delimiter='|')
    writer.writeheader()
    writer.writerows(reader)

# Add *** at the end of every line to prepare for latter processing and generate output.txt file.
with open('fout.txt', 'r') as istr, \
     open('output.txt', 'w') as ostr:
    for i, line in enumerate(istr): 
        line = line.rstrip('\n') + '***' 
        print(line, file=ostr)

# Load the output.txt file. 
def read_data():
	with open ("output.txt") as f:
		data = f.read()
		data = data.replace("\t", "")
		data = data.replace("\n", "")
		data = data.split("***")
		data = data[:-1]
		data[0] = data[0][1:]
		return data

# Transforms data into a list that contains dictionaries, each of which is all the info for one entity.
def make_dictionary_list():
	stack = [i.split("|") for i in read_data()]
	return [dict(zip(stack[0], stack[i])) for i in range(1, len(stack))]

searchstring = "search_strings"

# Get a list of dictionaries that only contain multiple search strings
def get_dict_list_multiple_only(function=make_dictionary_list):
	return [i for i in function()
	        if ";" in i.get(searchstring, "empty")]

# Make several dictionaries, each of which has one search strings from the multiple search strings
def get_multiple_ss_list():
	stack = []
	for i in get_dict_list_multiple_only():
		new_ss = i.get(searchstring).split("; ")
		for n in range(len(new_ss)):
			dictlist = i.copy()
			dictlist[searchstring] = new_ss[n] 
			stack.append(dictlist)
	return stack
#get_multiple_ss_list()

# Combine old and new dictionaries with only one string strings, and get rid of the dictionaries with multiple strings. 
def get_whole_list():
	#this function gets the entire list of dictionaries including the expanded multiple search strings
	lst = [entry for entry in make_dictionary_list()
	       if ";" not in entry.get(searchstring, "empty")]
	return lst + get_multiple_ss_list()

#pprint(get_whole_list())

def dict_rewrite(dictionary):
	string = dictionary.get("search_strings", "empty")
	entity_id = dictionary.get("entity_id", "empty")
	parent_id = dictionary.get("parent_id", "empty")
	entity_name = dictionary.get("entity_name", "empty")
	entity_type = dictionary.get("entity_type", "empty")
	domain= dictionary.get("domain", "empty")
	country = dictionary.get("country", "empty")
	wp3_search = dictionary.get("wp3_search", "empty")
	exchange = dictionary.get("exchange", "empty")
	company_sector = dictionary.get("company_sector", "empty")
	company_industry = dictionary.get("company_industry", "empty")
	index_component = dictionary.get("index_component", "empty")
	DUPLICATE_TICKER = dictionary.get("DUPLICATE_TICKER", "empty")
	return string + "|" + "entity_id=" + entity_id + "|" + "parent_id=" + parent_id + "|" + "entity_name=" + entity_name + "|" + "entity_type=" + entity_type + "|" + "domain=" + domain +"|" + "country=" + country + "|" + "wp3_search=" + wp3_search +"|" + "exchange=" + exchange + "|" + "company_sector=" + company_sector +"|" + "company_industry=" + company_industry + "|"+ "index_component=" + index_component + "|" + "DUPLICATE_TICKER=" + DUPLICATE_TICKER

# Below are functions to get different annotation type lists
# This gets the list of entities with type Stock
def get_stock_lst():
	return [dict_rewrite(dictionary) for dictionary in [
			 dictionary.copy() for dictionary in get_whole_list()
	         if dictionary.get("entity_type") == "Stock"]]
with open("../product/stock.lst", "w") as f:
	for i in get_stock_lst():
		f.write("%s\n"%i)

# This gets the list of entities with type Company
def get_company_lst():
	#this function gets the items when the annotation type is "Company"
	return [dict_rewrite(dictionary) for dictionary in [
			 dictionary.copy() for dictionary in get_whole_list()
	         if dictionary.get("entity_type") == "Company"]]
with open("../product/company.lst", "w") as f:
	for i in get_company_lst():
		f.write("%s\n"%i)

# This gets the list of entities with type Product
def get_product_lst():
	#this function gets the items when the annotation type is "Product"
	return [dict_rewrite(dictionary) for dictionary in [
			 dictionary.copy() for dictionary in get_whole_list()
	         if dictionary.get("entity_type") == "Product"]]
with open("../product/product.lst", "w") as f:
	for i in get_product_lst():
		f.write("%s\n"%i)

# This gets the list of entities with type Person
def get_person_lst():
	#this function gets the items when the annotation type is "Person"
	return [dict_rewrite(dictionary) for dictionary in [
			 dictionary.copy() for dictionary in get_whole_list()
	         if dictionary.get("entity_type") == "Person"]]
with open("../product/person.lst", "w") as f:
	for i in get_person_lst():
		f.write("%s\n"%i)

# This gets the list of entities with type Future
def get_future_lst():
	#this function gets the items when the annotation type is "Future"
	return [dict_rewrite(dictionary) for dictionary in [
			 dictionary.copy() for dictionary in get_whole_list()
	         if dictionary.get("entity_type") == "Future"]]
with open("../product/future.lst", "w") as f:
	for i in get_future_lst():
		f.write("%s\n"%i)

# This gets the list of entities with type ETF
def get_ETF_lst():
	#this function gets the items when the annotation type is "ETF"
	return [dict_rewrite(dictionary) for dictionary in [
			 dictionary.copy() for dictionary in get_whole_list()
	         if dictionary.get("entity_type") == "ETF"]]
with open("../product/ETF.lst", "w") as f:
	for i in get_ETF_lst():
		f.write("%s\n"%i)

# This gets the list of entities with type Forex
def get_forex_lst():
	#this function gets the items when the annotation type is "Forex"
	return [dict_rewrite(dictionary) for dictionary in [
			 dictionary.copy() for dictionary in get_whole_list()
	         if dictionary.get("entity_type") == "Forex"]]
with open("../product/forex.lst", "w") as f:
	for i in get_forex_lst():
		f.write("%s\n"%i)

# This gets the list of entities with type Index
def get_index_lst():
	#this function gets the items when the annotation type is "Index"
	return [dict_rewrite(dictionary) for dictionary in [
			 dictionary.copy() for dictionary in get_whole_list()
	         if dictionary.get("entity_type") == "Index"]]
with open("../product/index.lst", "w") as f:
	for i in get_index_lst():
		f.write("%s\n"%i)


