This script is used to update our database in GATE. 
For any questions, please contact Chong Zhang, zhangchong10@gmail.com

Requirements of the database format:

1. The current script works for Ross's data structure only. If more columns are added, corresponding changes need to be made in the script from line 64-77. The function dict_rewrite() takes care of adding or dropping columns. 

2. More importantly, in order for the script to extract entities from "NER - ANY OF THESE STRINGS", entity names must be separated from each other by a semicolon followed by a space. For example "$ANA; ANA:SM; BME:ANA; ANA.MC" is a valid structure, but "$ANA;ANA:SM;BME:ANA;ANA.MC" (missing space) or "$ANA, ANA:SM, BME:ANA, ANA.MC" (using comma instead of semicolon) are not valid. 

---------------------------------------------------------------------------------------------------------------------------------------------------

Steps to update the database:

1. Save database to a .csv file as whole_data.csv in the same folder with the python script lb_integrate_entities.py.

2. Run the script with Python3:
	2.1 open a terminal
	2.2 navigate to where you saved lb_integrate_entities.py
	2.3 type "python 3 lb_integrate_entities.py" in the terminal and hit enter

3. Two .txt files will be generated
	3.1 fout.txt has added the delimiter "|" between csv columns.
	3.2 output.txt has added "***" at the end of each row in the csv file.
	This step is to do some preprocessing and maintain the csv structure for later use. The generated files only serve as intermediate steps to get the final lists. 

4. After running the script, seven .lst files will also be generated. They are files that should be fed into GATE. These files are automatically updated (generated and replace the old ones) in their designated folder: .../LionbridgeCaseStudy/GATE_modules/gazetteer/product

5. Now the database is updated.

6. Before you run the GATE NER app to test the results, you need to reinitialize Lionbridge gazetteer so the newly updated lists can be loaded by GATE. 