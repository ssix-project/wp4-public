1. Upload the GATE format GS file which contains 'Mention' as one of the annotation for stock symbols and create a corpus.
2. Load the TwitIE application of lionbridge NER component.
3. Run the application over the corpus.
4. Open the Annotation difference tool and Set the key set as Test(Pre-annotated stock symbols) and Resp. set as default(annotated from lionbridge NER).
5. Compare to see the statistics.
