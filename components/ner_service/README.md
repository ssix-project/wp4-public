You need Java 1.8 and Maven 3.3. It may work with earlier versions of maven but that has not been tested. 

####Manual Run
- change to project directory `cd ner_service`
- type `mvn spring-boot:run`
- usage `curl -G -v "http://localhost:8085" --data-urlencode 'text=Apple Debt: Cheap Compared To Peers http://t.co/zgds6dBm1 $AAPL'`

####Docker Run
- build docker image `docker build -t docker.insight-centre.org:5000/ssix-ner .`
- run docker container `docker run -p 8085:8085 docker.insight-centre.org:5000/ssix-ner`

####DC/OS Deployment
- make sure the image is accessible in DC/OS. In our case the naming convention is `docker.insight-centre.org:5000/ssix-ner`
- add service to DC/OS `dcos marathon add marathon-ner.json`