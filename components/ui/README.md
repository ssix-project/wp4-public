# SSIX NLP UI

Generic UI that consumes the SSIX NLP REST API, for testing and providing quick demos.

Further details at [PIPELINE-206]https://jira.ssix-project.eu/browse/PIPELINE-206).

## Environment

This tool needs [`bower`](https://bower.io/) and  [`grunt`](http://gruntjs.com/) installed
to install all required dependencies:

1. `npm install`
2. `bower install`

## Build & development

1. Start the NLP microservice at `localhost:5000`.
2. Run `grunt serve` for running the ui 
   (it may require CORS disabled, use 
   [this extension](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi) 
   in Chrome for example.

## Testing

Running `grunt test` will run the unit tests with karma.

## Deploying

1. Customize where the NLP component will be running (`app/scripts/app.js:22`). 
2. Then run `grunt` for building the distribution.
3. Deploy it to any HTTP server (e.g., Apache or Nginx).
