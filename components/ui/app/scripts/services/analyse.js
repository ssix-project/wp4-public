'use strict';

/**
 * @ngdoc function
 * @name nlpUiApp.services:AnalyseService
 * @description
 * # AnalyseService
 * Service for analyzing of the nlpUiApp
 */
angular.module('nlpUiApp').service('AnalyseService', function($http, $q, $log, serviceUrl) {

  this.analyse = function (content) {
    var deferred = $q.defer();

    $http.post(serviceUrl, content)
      .then(
        function(response) {
          deferred.resolve(response.data);
        },
        function(response) {
          $log.error('Request to NLP service has failed');
          deferred.reject(response);
        }
      );

    return deferred.promise;
  };

});
