'use strict';

/**
 * @ngdoc overview
 * @name nlpUiApp
 * @description
 * # nlpUiApp
 *
 * Main module of the application.
 */
angular
  .module('nlpUiApp', [
    'ngAnimate',
    'ngResource',
    'ngSanitize',
    'ngMaterial'
  ])
  .factory('serviceUrl', ['$window', function serviceUrlFactory($window) {
    if ($window.location.port === 9000) { // when started with 'grunt serve'
      return 'http://localhost:5000/';
    } else {
      return '/endpoint'; // wherever it runs
    }
  }])
  .config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('orange');
    $mdThemingProvider.enableBrowserColor();
  });
