'use strict';

/**
 * @ngdoc function
 * @name nlpUiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nlpUiApp
 */
angular.module('nlpUiApp')
  .controller('MainCtrl', function ($scope, $mdToast, $mdBottomSheet, AnalyseService) {

    $scope.submit = function(content) {
      AnalyseService.analyse(content.text)
        .then(
          function(response) {
            $scope.result = response;
          },
          function() {
            $mdToast.show({
              hideDelay   : 5000,
              position    : 'bottom right',
              templateUrl : 'views/templates/error.html'
            });
          }
        );
    };

    $scope.showInfo = function() {
      $mdBottomSheet.show({
        templateUrl: 'views/templates/info.html'
      });
    };

  });
