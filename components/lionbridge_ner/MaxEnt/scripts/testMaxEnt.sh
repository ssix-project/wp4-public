#! /bin/bash

# Timo Järvinen, Lionbridge

export SCRIPTS=scripts
export DATA=.
export MAXENT=./tools/opennlp-maxent-3.0.0/output/

ARGCOUNT=3

if [ $# -ne "$ARGCOUNT" ] 
then
  echo "Usage: testMaxEnt.sh training_file model_file test_file"
  exit 0
fi  


$SCRIPTS/features.sh <$DATA/$3 | gawk ' BEGIN { FS="\t" } { print $2 }' >$DATA/test/testfile.txt 
$SCRIPTS/features.sh <$DATA/$1 >$DATA/test/trainfile.txt 

javac -classpath .:$MAXENT/maxent-3.0.0.jar MaxEntTrainer.java
java -classpath .:$MAXENT/maxent-3.0.0.jar MaxEntTrainer $DATA/test/trainfile.txt $DATA/$2
javac -classpath .:$MAXENT/maxent-3.0.0.jar MaxEntPredictor.java
java -classpath .:$MAXENT/maxent-3.0.0.jar MaxEntPredictor $DATA/$2 >test/predicted.txt

paste -d"	" $DATA/test/predicted.txt $DATA/$3 >$DATA/test/results.txt

python $SCRIPTS/f1_lb.py $DATA/test/results.txt  2>/dev/null


