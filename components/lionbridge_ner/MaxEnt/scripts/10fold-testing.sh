#! /bin/bash

# Timo Järvinen, Lionbridge

export SCRIPTS=scripts

# preprocessing the test data by converting the
# Activity Type markup spans to separate tags to be used as features
# in the MaxEnt model

gawk ' { 
 gsub (/<AT_[^>]+>/, "& "); 
 gsub (/<\/AT_[^>]+>/, ""); print 
 }' <ActivityType_batch5_gold.txt >ActivityType_batch5_gold.tmp

# Note: NER module implemented in GATE not included in the pipe.
# Model trained and tested without Product and Company features. 

         COUNTER=1
         while [  $COUNTER -lt 11 ]; do
             echo Round $COUNTER in progress...
# split the data into train and test samples
             $SCRIPTS/split_in2.sh
# train OpenNLP MaxEnt classifier and test the model or test sample
# write the results of all iterations
	     $SCRIPTS/testMaxEnt.sh test/train2.txt ActivityType_Model_batch5.bin.gz test/test2.txt | egrep '^Average' | sed 's/Average: weighted, //g' >> test/10fold_results.txt
	     let COUNTER=COUNTER+1
         done
