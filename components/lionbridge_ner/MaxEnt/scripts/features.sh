#! /bin/bash

# Timo Järvinen, Lionbridge

# Testing with different feature combinations 

export SCRIPTS=scripts

#gawk ' { gsub (/[ ]+/, " "); print }' 

# baseline
# gawk ' { print $0 }'

# normalize URLs
#gawk ' { gsub (/http[s]*:\/[/.A-Za-z0-9…-]+/, "<url>..</url>"); print }'

# remove feats: Company_ Product_
#gawk ' { gsub (/Company_ /, ""); gsub (/Product_ /, ""); print }'

# URL normalization + tokenization of sentence punctuation
gawk ' { 
gsub (/http[s]*:\/[/.A-Za-z0-9…-]+/, "_URL");
gsub (/([.?!]+)/, " &"); 
#gsub (/\&quot;/, " \" "); 
#gsub (/\&apos;/, " _"); 
print }' 


