# F1, Precision and Recall
# Written by Enrico Santus for Lionbridge
# Based on http://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html



import codecs
import numpy as np
from docopt import docopt
from sklearn.metrics import precision_recall_fscore_support


def main():
    """
    Return the Precision, Recall and F1 for multiclass classifier, providing a file containing PREDICTED and GOLDSTANDARD
    lables in two tab-separated columns. The program can be called with optional options to weight the average.
    """

    # Get the arguments
    args = docopt("""Return the Precision, Recall and F1 for multiclass classifier, providing a file containing PREDICTED and GOLDSTANDARD
    lables in two tab-separated columns. The program can be called with optional options to weight the average.

    Usage:
        f1_lb.py <test_results_file> [<average>]

        <test_results_file> = the test set result file containing the column PREDICTED tab separated from GOLDSTANDARD
        <average> = string containing either: binary | micro | macro | weighted | samples
    """)

    test_results_file = args['<test_results_file>']
    avg = args['<average>']

    # Load the datasets
    with codecs.open(test_results_file) as f_in:
        test_set = [tuple(line.strip().split('\t')) for line in f_in]

    lbls = list(set([item[0] for item in test_set] + [item[1] for item in test_set]))

    #print lbls

    x_test = [item[0] for item in test_set]
    y_test = [item[1] for item in test_set]

    #print x_test, y_test

    try:
            if avg != "binary" and avg != "macro" and avg != "micro" and avg != "weighted" and avg != "samples":
                avg = "weighted"
            p, r, f1, support = precision_recall_fscore_support(y_test, x_test, labels=lbls, average=avg)
    except:
        print "EXCEPTION!!"
        p = r = f1 = 0.0

    print 'Average: %s, Precision: %.3f, Recall: %.3f, F1: %.3f' % (avg, p, r, f1)



if __name__ == '__main__':
    main()

