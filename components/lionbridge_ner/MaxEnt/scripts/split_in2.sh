#! /bin/bash

# Timo Järvinen, Lionbridge


export SCRIPTS=scripts


gawk  'BEGIN { N=1999; m=1699; test="test/test2.txt"; train="test/train2.txt"; srand()
         do{ lnb = 1 + int(rand()*N)
             if ( !(lnb in R) ) {
                 R[lnb] = 1
                 ct++ }
         } while (ct<m)
  } { if (R[NR]==1) print >train
      else          print >test
  }' <ActivityType_batch5_gold.tmp



