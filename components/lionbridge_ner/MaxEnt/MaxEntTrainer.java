/** 
5. FileEventStream (file format is: each line one outcome given space delimited context
6. OnePassDataIndexer of EventStream, the example is public OnePassDataIndexer(EventStream es, int cutoff){ ... } 
7. GIS.trainModel, the example is public static MaxentModel trainModel(DataIndexer di, int iterations) {  ...  } 
8. Write trained model to file
9. Load model from file
**/

/**
public static MaxentModel trainModel(DataIndexer di, int iterations) {  ...  } 

public OnePassDataIndexer(EventStream es, int cutoff){ ... } 
OnePassDataIndexer(EventStream events)

File outputFile = new File(modelFileName+".bin.gz"); 
GISModelWriter writer = new SuffixSensiiveGISModelWriter(model, outputFile); 
writer.persist(); 

GISModel m = new SuffixSensitiveGISModelReader(new File(modelFileName)).getModel(); 

**/

import java.lang.*;
import opennlp.model.*;
import opennlp.maxent.*;
import opennlp.maxent.io.*;
//import opennlp.tool.*;
import java.io.*;
class MaxEntTrainer {


	
   public static void main (String args[]) {
	
     //System.out.println("Hello World!");   //Displays the enclosed String on the Screen Console
       //         String trainFile = null;
       //	String modelFile = null;
       //
       //	if (3 > args.length) {
       //		modelFile = args[1];
       //		trainFile = args[2];
       //	}
	
		

	try{
	
	//read in training data from file
	 String fname = args[0];
	//File train = new File(fname);
	 FileEventStream fes = new FileEventStream(fname);
	 OnePassDataIndexer thing = new OnePassDataIndexer(fes);
	 GIS giser = new GIS();
	 //train model
	 GISModel myFirstModel = giser.trainModel(100, thing);
	 
	 //write trained model to disk
	//String modelFileName = "POSModel";
	String modelFileName = args[1];
	File outputFile = new File(modelFileName); 
	GISModelWriter writer = new SuffixSensitiveGISModelWriter(myFirstModel, outputFile); 
	//GISModelWriter writer = new GISModelWriter(myFirstModel); 
	writer.persist(); 
	
	//load model from disk
	//GISModel m = new SuffixSensitiveGISModelReader(new File(modelFileName)).getModel(); 
	 }
	 catch(IOException e){
  e.printStackTrace();
		}

   }
   
 }
