/** 
9. Load model from file
**/

/**
To ask the model whether it believes that Terrence is a name or not, you send a String[] with all of the features (such as those discussed above) to the model by calling the method: 

public double[] eval(String[] context); 
The double[] which you get back will contain the probabilities of the various outcomes which the model has assigned based on the features which you sent it.  The indexes of the double[] are actually paired with outcomes.  For example, the outcomes associated with the probabilites might be "TRUE" for index 0 and "FALSE" for index 1.  To find the String name of a particular index outcome, call the method: 

public String getOutcome(int i); 
Also, if you have gotten back double[] after calling eval and are interested in only the outcome which the model assigns the highest probability, you can call the method: 

public String getBestOutcome(double[] outcomes); 
And this will return the String name of that most likely outcome. 

**/

import java.lang.*;
import opennlp.model.*;
import opennlp.maxent.*;
import opennlp.maxent.io.*;
//import opennlp.tool.*;
import java.io.*;
class MaxEntPredictor {

   public static void main (String args[]) {
	
     //System.out.println("Hello World!");   //Displays the enclosed String on the Screen Console
	
	try{
	
	//load model from disk
	String modelFileName = args[0];
	GISModelReader loader = new SuffixSensitiveGISModelReader(new File(modelFileName));
	MaxentModel m = loader.getModel();
	
	//predict from file
	String         line;


	//Use the below file to test that re-training/updating the model did not have a negative impact.
	FileInputStream fis = new FileInputStream("test/testfile.txt");
	//FileInputStream fis = new FileInputStream("MLformatted_pl-ipipan-rosetta-train.conll");
	BufferedReader br = new BufferedReader(new InputStreamReader(fis));
	while ((line = br.readLine()) != null) {
	
	String[] testCaseContext = line.split(" ");
	String prediction = m.getBestOutcome(m.eval(testCaseContext));
	System.out.println(prediction);
	}

	br.close();
	
	 }
	 catch(IOException e){
		e.printStackTrace();
		}
	//catch(ClassNotFoundException e){
	//	e.printStackTrace();
	//	}

   }
   
 }
