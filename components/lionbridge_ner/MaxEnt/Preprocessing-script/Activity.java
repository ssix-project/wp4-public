
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.*;
import java.io.*;

public class Activity {
   
   private static final String file1 = "ActivityType_batch5_2k.txt";
    
   public static void main(String[] args) throws IOException {
    BufferedReader br = null;
    FileReader fr = null;
    Pattern p1 = Pattern.compile(".*\\s(hiring|employs|employ|employing)\\s.*",Pattern.CASE_INSENSITIVE);
    Pattern p2 = Pattern.compile(".*\\s(appoint|appointed|appointing|appoints|nominate|nominates|nominated|nominating|join|joins|joined|joining|hires|hired|CEO)\\s.*",Pattern.CASE_INSENSITIVE);
    Pattern p3 = Pattern.compile(".*\\s(downsize|downsized|downsizes|downsizing|cut|cuts|cutting|shrink|shrinks|shrinked|shrinking|reduce|reduces|reduced|reducing|streamline|streamlines|streamlined|streamlining|scale down|scales down|scaling down|scaled down|layoff|layoffs)\\s.*",Pattern.CASE_INSENSITIVE);
    Pattern p4 = Pattern.compile(".*\\s(release|releases|released|releasing|launch|launches|launched|launching|unveil|unveils|unveiled|unveiling|introduce|introduces|introduced|introducing)\\s.*",Pattern.CASE_INSENSITIVE);
    Pattern p5 = Pattern.compile(".*\\s(participate|participates|participating|sponsor|sponsors|sponsoring|sponsored by)\\s.*",Pattern.CASE_INSENSITIVE);
    Pattern p6 = Pattern.compile(".*\\s(purchase|purchased|acquire|acquires|acquired|achieve|achieved|invest|invests|invested|investing|deal|win|investment|acquisition)\\s.*",Pattern.CASE_INSENSITIVE);
    Pattern p7 = Pattern.compile(".*\\s(announce|announces|announced)\\s.*",Pattern.CASE_INSENSITIVE);
    try {
        
        fr = new FileReader(file1);
        br = new BufferedReader(fr);
        
        String Line;
        
        br = new BufferedReader(new FileReader(file1));
        
        PrintWriter writer = new PrintWriter("ActivityType_Annotated.txt", "UTF-8");
        
        while ((Line = br.readLine()) != null) {
            Matcher m1=p1.matcher(Line);
            Matcher m2=p2.matcher(Line);
            Matcher m3=p3.matcher(Line);
            Matcher m4=p4.matcher(Line);
            Matcher m5=p5.matcher(Line);
            Matcher m6=p6.matcher(Line);
            Matcher m7=p7.matcher(Line);

            
            if(m1.find()){
            
            String r1=m1.replaceAll("<AT_Hire>$1</AT_Hire>");
            String k1=r1.replaceAll("<AT_Hire>","");
            k1=k1.replaceAll("</AT_Hire>","");
            writer.println(Line.replaceAll(k1,r1));
            }
            else if(m2.find()){
                
                String r2=m2.replaceAll("<AT_App>$1</AT_App>");
                String k2=r2.replaceAll("<AT_App>","");
                k2=k2.replaceAll("</AT_App>","");
                writer.println(Line.replaceAll(k2,r2));
            
            }
            else if(m3.find()){
                
                String r3=m3.replaceAll("<AT_Down>$1</AT_Down>");
                String k3=r3.replaceAll("<AT_Down>","");
                k3=k3.replaceAll("</AT_Down>","");
                writer.println(Line.replaceAll(k3,r3));
                
            }
            else if(m4.find()){
                
                String r4=m4.replaceAll("<AT_Rel>$1</AT_Rel>");
                String k4=r4.replaceAll("<AT_Rel>","");
                k4=k4.replaceAll("</AT_Rel>","");
                writer.println(Line.replaceAll(k4,r4));
                
            }
            else if(m5.find()){
                
                String r5=m5.replaceAll("<AT_EvP>$1</AT_EvP>");
                String k5=r5.replaceAll("<AT_EvP>","");
                k5=k5.replaceAll("</AT_EvP>","");
                writer.println(Line.replaceAll(k5,r5));
                
            }
            else if(m6.find()){
                
                String r6=m6.replaceAll("<AT_Deal>$1</AT_Deal>");
                String k6=r6.replaceAll("<AT_Deal>","");
                k6=k6.replaceAll("</AT_Deal>","");
                writer.println(Line.replaceAll(k6,r6));
                
            }
            else if(m7.find()){
                
                String r7=m7.replaceAll("<AT_PuA>$1</AT_PuA>");
                String k7=r7.replaceAll("<AT_PuA>","");
                k7=k7.replaceAll("</AT_PuA>","");
                writer.println(Line.replaceAll(k7,r7));
                
            }
            else{
                writer.println(Line);
            }
            
        }
        writer.close();
        
    } catch (IOException e) {
        
        e.printStackTrace();
        
    } finally {
        
        try {
            
            if (br != null)
                br.close();
                
                if (fr != null)
                    fr.close();
                    
                    } catch (IOException ex) {
                        
                        ex.printStackTrace();
                        
                    }
        
    }

   }
}
