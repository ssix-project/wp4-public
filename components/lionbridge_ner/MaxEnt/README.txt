Liobridge User Case

The files in this folder are related to preliminary investigation of
using statistical classifier (OpenNLP Maximum Entropy model) as an input
for Company Activity Indexes IR task.

The proposed solution consist of three consecutive filtering steps:

1) Filter using CAI keywords

For this purpose (the input data being Tweets) we would use the
keywords https://jira.ssix-project.eu/browse/DATA-64

2) Filter using CAI:IR task keywords

3) Use statistical classifier to further filter out irrelevant tweets
and classify relevant tweets to appropriate Activity Types

For the third step we have created a test implementation of three
distinct components to be run in sequence:

1)	Activity Type filtering component implemented in java
input: text  (Tweets or sentences)
output: Activity Type annotated  output

2)	NER module implemented in GATE consisting of
-	lookup files:   company.lst, product.lst  (aka Lionbridge entity DB)
-	jape rule component implemented in stock.jape file

input: text (Tweets or sentences)
output: text annotated with specific Product and Company mentions

3)	 Activity Type classifier
OpenNLP MaxEnt language model trained on our validated benchmark data.

input: text (Tweets or sentences)  with space-separated text tokens,
Product, Company (if present) and Activity Type markups as features
output: input units (Tweets or sentences) classified per Activity Type

The script
./scripts/10fold-testing.sh
takes the annotated data set "ActivityType_batch5_gold.txt", splits it
into train and test sets (test/trainfile.txt and test/testfile.txt,
respectively).
A model file "ActivityType_Model_batch5.bin.gz" is created on the training
data and it is applied to the test data. The test cycle is iterated 10
times and the results are written in "test/10fold_results.txt". Only the
model and temporary files from the last iteration are retained as we were
interested in the test results only.

Running the scripts requires standard unix/Linux tools, python and
OpenNLP with Maximum Entropy Package
(https://sourceforge.net/projects/maxent/files/)
The path to maxent is set in file ./scripts/testMaxEnt.sh
MAXENT=/home/shared/opennlp/opennlp-maxent-3.0.0/output
