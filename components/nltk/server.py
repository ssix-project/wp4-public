#!/usr/bin/python

import sys
import time
import json

from flask import Flask, request, jsonify
from analyzer import NLTKSentimentAnalyzer


app = Flask("nltk")
analyzer = NLTKSentimentAnalyzer()


@app.route("/", methods=["POST"])
def serve():
    item = request.get_json()
    data = item.get('content')
    sentiment = analyzer.get_polarity(data)
    app.logger.debug("sentiment for '%s' is %f" % (data, sentiment))
    response = {
        "data": data,
        "sentiment": sentiment,
        "timestamp": time.time()
    }
    return jsonify(response)


@app.route("/", methods=["HEAD"])
def head():
    return ""


if __name__ == "__main__":
    port = int(sys.argv[1]) if len(sys.argv) > 1 else None
    app.run(host="0.0.0.0", port=port, debug=True)
