import os
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer


class NLTKSentimentAnalyzer:

    def __init__(self):
        nltk_data = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "nltk_data"))
        nltk.data.path.append(nltk_data)
        self.sia = SentimentIntensityAnalyzer()

    def get_polarity(self, content=""):
        content = content if type(content) == str else ""
        polarity = self.sia.polarity_scores(content)
        return polarity["compound"]
