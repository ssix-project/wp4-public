
from nose.tools import assert_true, assert_equals
from analyzer import NLTKSentimentAnalyzer

analyzer = NLTKSentimentAnalyzer()

def test_empty():
    assert_equals(0.0, analyzer.get_polarity(""))

def test_none():
    assert_equals(0.0, analyzer.get_polarity(None))

def test_pipeline_54():
    assert_almost_equal(1.0, analyzer.get_polarity("i am very happy today my friend"), 0.2)

def test_bad():
    assert_almost_equal(-0.6, analyzer.get_polarity("this stock sucks!"), 0.2)

def test_pipeline_58_01():
    assert_almost_equal(0.4, analyzer.get_polarity("Twenty-First Century Fox Inc $FOXA Shares Up 0.8% on Analyst Upgrade https://t.co/QTKeySsv1n"), 0.2)

def test_pipeline_58_02():
    assert_almost_equal(0.1, analyzer.get_polarity("Apple With A 10% Yield https://t.co/UorjLEdgb7 $AAPL #APPLE"), 0.2)

def test_pipeline_58_03():
    assert_almost_equal(-0.2, analyzer.get_polarity("$AAPL Apple Inc. (AAPL) Scales Down iPhone Orders Rattling Suppliers https://t.co/ZJCza1cz9z"), 0.2)

def test_pipeline_58_04():
    assert_almost_equal(0.5, analyzer.get_polarity("$AAPL Dont overlook the H&amp;S Variant already in progress. Its the right shoulder of the 1 ur trying to justify https://t.co/6NURISWctD"), 0.2)

def test_pipeline_58_05():
    assert_almost_equal(-0.2, analyzer.get_polarity("Weekly #chart $AAPL showing #headandshoulders confirmation https://t.co/nO2ediy0HW"), 0.2)

def test_pipeline_58_06():
    assert_almost_equal(-0.2, analyzer.get_polarity("$QQQ Minor 5th wave may have another leg down today. Door still open for a larger degree 5th wave down tomorrow. https://t.co/PzCaeXY1JJ"), 0.2)

def test_pipeline_58_07():
    assert_almost_equal(0.6, analyzer.get_polarity("Yes. But I'm long here. https://t.co/cIBUvWynFv"), 0.2)

def test_pipeline_58_08():
    assert_almost_equal(-0.2, analyzer.get_polarity("Apple Inc. $AAPL Cut to Neutral at Rosenblatt Securities https://t.co/Vnaga1bBkJ"), 0.25)

def test_pipeline_58_09():
    assert_almost_equal(0.3, analyzer.get_polarity("Cisco Systems, Inc. $CSCO Shares Bought by Renaissance Investment Group https://t.co/Wu71FBSRal"), 0.1)

def test_pipeline_58_10():
    assert_almost_equal(-0.5, analyzer.get_polarity("$AAPL Is It Time to Panic About Apple Inc.? - https://t.co/nrHiNHD3ih"), 0.1)


def assert_almost_equal(desired, actual, margin=0.0):
    min = desired - margin if (desired - margin) > -1.0 else -1.0
    max = desired + margin if (desired + margin) < 1.0 else 1.0
    assert_true(min <= actual <= max, msg="%1.4f is not in the range (%1.2f,%1.2f)" % (actual, min, max))

