package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.nlp.api.SentimentClass;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wikier on 08/02/16.
 */
public class SentimentClassTest {

    @Test
    public void testOrder() {
        final SentimentClass[] values = SentimentClass.values();
        Assert.assertEquals(8, values.length);

        Assert.assertEquals(SentimentClass.StrongNegative, values[0]);
        Assert.assertEquals(SentimentClass.MediumNegative, values[1]);
        Assert.assertEquals(SentimentClass.WeakNegative, values[2]);
        Assert.assertEquals(SentimentClass.Neutral, values[3]);
        Assert.assertEquals(SentimentClass.WeakPositive, values[4]);
        Assert.assertEquals(SentimentClass.MediumPositive, values[5]);
        Assert.assertEquals(SentimentClass.StrongPositive, values[6]);
        Assert.assertEquals(SentimentClass.Irrelevant, values[7]);

    }
}
