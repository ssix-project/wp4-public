package io.redlink.ssix.pipeline.nlp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthAbsoluteEven;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;
import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;
import org.jooq.lambda.tuple.Tuple;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Analyzers benchmark test, see PIPELINE-47 for further details
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(Parameterized.class)
//@ContextConfiguration("classpath:context.xml")
public class AnalyzersBenchmarkIT {

    private static final Logger log = LoggerFactory.getLogger(AnalyzersBenchmarkIT.class);

    private static List<Content> data;

    private final String name;
    private final SentimentAnalyzer instance;

    private static ObjectMapper mapper = new ObjectMapper();

    private static final List<Tuple> report = new ArrayList<>();

    @Parameterized.Parameters
    public static Set<Map.Entry<String, SentimentAnalyzer>> configs() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"context.xml"});
        final Map<String, SentimentAnalyzer> sentimentAnalyzers = BeanFactoryUtils.beansOfTypeIncludingAncestors(context, SentimentAnalyzer.class);

        for (String name: new ArrayList<>(sentimentAnalyzers.keySet())) {
            final SentimentAnalyzer analyzer = sentimentAnalyzers.get(name);
            if (sentimentAnalyzers.get(name) == null) {
                log.warn("{} was null, removing it from the benchmark...", name);
                sentimentAnalyzers.remove(name);
            }
            if (analyzer instanceof External) {
                if (!((External)analyzer).isAvailable()) {
                    log.warn("{} ({}) is not available, removing it from the benchmark...", name, analyzer.getClass().getCanonicalName());
                    sentimentAnalyzers.remove(name);
                }
            }
        }

        log.info("Benchmarking against {} sentiment analyzer implementations:", sentimentAnalyzers.size());
        for (Map.Entry<String, SentimentAnalyzer> entry : sentimentAnalyzers.entrySet()) {
            log.info("  - {} ({})", entry.getKey(), entry.getValue().getClass().getCanonicalName());
        }

        return sentimentAnalyzers.entrySet();
    }

    @BeforeClass
    public static void prepare() throws IOException {
        data = mapper.readValue(AnalyzersBenchmarkIT.class.getResourceAsStream("/tweets.json"), new TypeReference<List<Content>>(){});
        log.info("loaded {} tweets as test data", data.size());
    }

    @AfterClass
    public static void report() {
        final V2_AsciiTable table = new V2_AsciiTable();
        table.addRule();
        table.addRow("analyzer name", "class", "running mode", "count", "total time", "avg. sentiment");
        table.addRule();
        for (Tuple item : report) {
            table.addRow(item.toList());
            table.addRule();
        }

        final V2_AsciiTableRenderer renderer = new V2_AsciiTableRenderer();
        renderer.setTheme(V2_E_TableThemes.UTF_LIGHT.get());
        renderer.setWidth(new WidthAbsoluteEven(160));
        System.out.println(renderer.render(table));
    }

    public AnalyzersBenchmarkIT(Map.Entry<String, SentimentAnalyzer> analyzerEntry) {
        name = analyzerEntry.getKey();
        instance = analyzerEntry.getValue();
        log.info("testing against {} analyzer ({})...", name, instance.getClass().getCanonicalName());
    }

    @Before
    public void setup() throws IOException {
        Assume.assumeNotNull(instance);
        Assume.assumeTrue(data.size() > 0);
    }

    @After
    public void destroy() {

    }

    private void reporting(String name, Class clazz, String runningMode, int count, long time, int sentiments) {
        final String formattedTime = String.format("%dm %02ds %02dms", time / 60000, (time % 60000) / 1000, time % 1000);
        final int avg = Math.round(sentiments / count);
        log.info("estimated time to process {} tweets with {} was {} ({})", data.size(), name, formattedTime, runningMode);
        log.debug("average time = {}ms, average sentiment = {}", time / count, avg);
        report.add(Tuple.tuple(name, clazz.getSimpleName(), runningMode, count, formattedTime, avg));
    }

    @Test
    public void testMonoThread() {
        int count = 0;
        int aggregatedSentiments = 0;

        long startTime = System.currentTimeMillis();

        for(Content tweet: data) {
            final Sentiment sentiment = instance.analyze(tweet);
            aggregatedSentiments += sentiment.value();
            count++;
            if (count % 50 == 0) {
                log.debug("Processed {} items with {} (mono-threaded)", count, name);
            }
        }

        final long estimatedTime = System.currentTimeMillis() - startTime;

        reporting(name, instance.getClass(), "mono-threaded", count, estimatedTime, aggregatedSentiments);

        Assert.assertEquals(data.size(), count);
    }

    @Test
    public void testMultiThread() {
        final int parallelism = 8;
        final AtomicInteger count = new AtomicInteger(0);
        final AtomicInteger aggregatedSentiments = new AtomicInteger(0);

        final ExecutorService executor = Executors.newFixedThreadPool(parallelism);

        long startTime = System.currentTimeMillis();
        
        Collection<Future> futures = new ArrayList<>();
        for (Content tweet: data) {
            Callable<Sentiment> task = new AnalyzeTweetTask(tweet, instance);
            Future future = executor.submit(task);
            futures.add(future);
        }

        for(Future future : futures) {
            try {
                final Sentiment sentiment = (Sentiment) future.get();
                count.incrementAndGet();
                aggregatedSentiments.addAndGet(sentiment.value());
                if (count.get() % 50 == 0) {
                    log.debug("Processed {} items with {} (multi-threaded)", count.get(), name);
                }
            } catch (InterruptedException | ExecutionException e) {
                log.error("Error: {}", e.getMessage(), e);
            }
        }

        final long estimatedTime = System.currentTimeMillis() - startTime;

        reporting(name, instance.getClass(), "multi-threaded", count.get(), estimatedTime, aggregatedSentiments.get());

        Assert.assertEquals(data.size(), count.get());

        executor.shutdown();
    }

    private class AnalyzeTweetTask implements Callable<Sentiment> {

        private final Content tweet;
        private final SentimentAnalyzer analyzer;

        public AnalyzeTweetTask(final Content tweet, final SentimentAnalyzer analyzer) {
            this.tweet = tweet;
            this.analyzer = analyzer;
        }

        @Override
        public Sentiment call() throws Exception {
            return analyzer.analyze(tweet);
        }

    }

}
