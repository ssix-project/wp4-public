package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.model.Target;
import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Random;

/**
 * Sentiment Function test
 *
 * @author Sergio Fernándezx
 */
public class SentimentFunctionTests {

    private final Random random = new Random();

    @Test
    public void testEmptyTargets() {
        final Content tweet = new Content();
        final Sentiment sentiment = new Sentiment(generatedRandomSentimentValue());
        final SentimentAnalyzer analyzer = Mockito.mock(SentimentAnalyzer.class);
        Mockito.when(analyzer.analyze(tweet)).thenReturn(sentiment);

        final SentimentFunction function = new SentimentFunction();
        function.setDomainSentimentAnalyzer(analyzer);
        function.setGenericSentimentAnalyzer(analyzer);
        Assert.assertTrue(function.call(tweet).getTargets().isEmpty());
    }

    @Test
    public void testTargetsFromNerEmptyFromSA() {
        final Content tweet = new Content();
        tweet.addTarget("foo");
        final Sentiment sentiment = new Sentiment(generatedRandomSentimentValue());
        final SentimentAnalyzer analyzer = Mockito.mock(SentimentAnalyzer.class);
        Mockito.when(analyzer.analyze(tweet)).thenReturn(sentiment);

        final SentimentFunction function = new SentimentFunction();
        function.setDomainSentimentAnalyzer(analyzer);
        function.setGenericSentimentAnalyzer(analyzer);
        Assert.assertEquals(1, function.call(tweet).getTargets().size());
    }

    @Test
    public void testTargetsFromNerAndSA() {
        final Content tweet = new Content();
        tweet.addTarget("foo");
        final Sentiment sentiment = new Sentiment(generatedRandomSentimentValue());
        sentiment.addTarget("bar", generatedRandomSentimentValue());
        final SentimentAnalyzer analyzer = Mockito.mock(SentimentAnalyzer.class);
        Mockito.when(analyzer.analyze(tweet)).thenReturn(sentiment);

        final SentimentFunction function = new SentimentFunction();
        function.setDomainSentimentAnalyzer(analyzer);
        function.setGenericSentimentAnalyzer(analyzer);
        Assert.assertEquals(2, function.call(tweet).getTargets().size());
    }

    @Test
    public void testTargetsFromNerAndSAOverlap() {
        final Content tweet = new Content();
        tweet.addTarget("foo");
        tweet.addTarget("bar");
        final Sentiment sentiment = new Sentiment(generatedRandomSentimentValue());
        sentiment.addTarget("bar", generatedRandomSentimentValue());
        final SentimentAnalyzer analyzer = Mockito.mock(SentimentAnalyzer.class);
        Mockito.when(analyzer.analyze(tweet)).thenReturn(sentiment);

        final SentimentFunction function = new SentimentFunction();
        function.setDomainSentimentAnalyzer(analyzer);
        function.setGenericSentimentAnalyzer(analyzer);
        Assert.assertEquals(2, function.call(tweet).getTargets().size());
    }

    @Test
    public void testTargetsFromEntityDB() {
        final Content tweet = new Content();
        tweet.addTarget("Apple Inc");
        final Target target = new Target();
        target.setEntity_name("Apple Inc.");
        target.setDomain("Finance");
        tweet.addTarget(target);

        final Sentiment sentiment = new Sentiment(generatedRandomSentimentValue());
        final SentimentAnalyzer analyzer = Mockito.mock(SentimentAnalyzer.class);
        Mockito.when(analyzer.analyze(tweet)).thenReturn(sentiment);

        final SentimentFunction function = new SentimentFunction();
        function.setDomainSentimentAnalyzer(analyzer);
        function.setGenericSentimentAnalyzer(analyzer);
        Assert.assertEquals(2, function.call(tweet).getTargets().size());
    }


    private int generatedRandomSentimentValue() {
        return random.nextInt(Sentiment.MAX_VALUE - Sentiment.MIN_VALUE + 1) + Sentiment.MIN_VALUE;
    }

}
