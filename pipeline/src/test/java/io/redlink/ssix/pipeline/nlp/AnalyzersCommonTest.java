package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;

/**
 * Analyzers common (and basic) tests, see PIPELINE-47 for further details
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(Parameterized.class)
public class AnalyzersCommonTest {

    private static final Logger log = LoggerFactory.getLogger(AnalyzersCommonTest.class);

    private final SentimentAnalyzer instance;

    @Parameterized.Parameters
    public static Collection<SentimentAnalyzer> configs() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"context.xml"});
        final Map<String, SentimentAnalyzer> sentimentAnalyzers = BeanFactoryUtils.beansOfTypeIncludingAncestors(context, SentimentAnalyzer.class);
        final Collection<SentimentAnalyzer> analyzers = sentimentAnalyzers.values();
        analyzers.remove(null);
        return analyzers;
    }

    public AnalyzersCommonTest(SentimentAnalyzer analyzer) {
        instance = analyzer;
        log.info("testing against an instance of {}...", instance.getClass().getCanonicalName());
    }

    @Before
    public void setup() throws IOException {
        Assume.assumeNotNull(instance);
        //instance.init();
    }

    @After
    public void destroy() {
        //instance.destroy();
    }

    @Test
    public void testEmpty() {
        final Sentiment sentiment = instance.analyze(new Content());
        Assert.assertNotNull(sentiment);
        Assert.assertEquals(0, sentiment.value());
    }

    @Test
    public void testNull() {
        final Sentiment sentiment = instance.analyze(null);
        Assert.assertNotNull(sentiment);
        Assert.assertEquals(0, sentiment.value());
    }

}
