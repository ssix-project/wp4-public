package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.impl.BrexitSentimentAnalyzer;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * Integration tests for the sentiment analyzer Brexit implementation
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:context.xml")
public class BrexitSentimentAnalyzerIT {

    @Autowired
    private BrexitSentimentAnalyzer analyzer;

    @Before
    public void before() {
        Assume.assumeNotNull(analyzer);
        Assume.assumeNotNull(analyzer.analyze(null));
    }

    @Test
    public void testVeryPolarizedSentimentTweet() {
        final Content negativeTweet = new Content();
        negativeTweet.setContent("‘Brexit’ is strengthening the forces that already haunt the global economy http://ift.tt/28XtFCi  #BREXIT #StrongerIn #No2EU #EUref #Leave");

        final Content positiveTweet = new Content();
        positiveTweet.setContent("RBS and Lloyds share sale to be put on hold by UK Government in Brexit aftermath");

        final Integer negativeSentimentValue = analyzer.analyze(negativeTweet).value();
        final Integer positiveSentimentValue = analyzer.analyze(positiveTweet).value();

        Assert.assertEquals(new Integer(-1000), negativeSentimentValue);
        Assert.assertEquals(new Integer(1000), positiveSentimentValue);
    }

}

