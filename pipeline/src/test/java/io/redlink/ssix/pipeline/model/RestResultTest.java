package io.redlink.ssix.pipeline.model;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * RestResult unit and marshalling tests
 *
 * @author Sergio Fernández
 */
public class RestResultTest {

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testPayload() throws IOException {
        final String data = "{\n" +
                "  \"data\": \"BBC do not even produce news anymore it is just left wing, anti Brexit, anti Trump pro EU pro immigration\", \n" +
                "  \"sentiment\": -0.5574, \n" +
                "  \"targets\": {\n" +
                "    \"nwsa\": {\n" +
                "      \"sentiment\": -0.5574\n" +
                "    }, \n" +
                "    \"pro\": {\n" +
                "      \"sentiment\": -0.5574\n" +
                "    }, \n" +
                "    \"wing\": {\n" +
                "      \"sentiment\": -0.5574\n" +
                "    }\n" +
                "  }, \n" +
                "  \"timestamp\": 1485039219.614935\n" +
                "}";
        final RestResult result = mapper.readValue(data, RestResult.class);
        Assert.assertEquals(-0.5574, result.getSentiment(), 0.0);
        Assert.assertEquals("BBC do not even produce news anymore it is just left wing, anti Brexit, anti Trump pro EU pro immigration", result.getData());
        Assert.assertEquals(3, result.getTargets().size());
        Assert.assertEquals(-0.5574, result.getTargets().get("nwsa").getSentiment(), 0.0);
        Assert.assertEquals(-0.5574, result.getTargets().get("pro").getSentiment(), 0.0);
        Assert.assertEquals(-0.5574, result.getTargets().get("wing").getSentiment(), 0.0);
    }

    @Test
    public void testPayloadEmptyTargets() throws IOException {
        final String data = "{\n" +
                "  \"data\": \"BBC do not even produce news anymore it is just left wing, anti Brexit, anti Trump pro EU pro immigration\", \n" +
                "  \"sentiment\": -0.5574, \n" +
                "  \"targets\": {\n" +
                "  }, \n" +
                "  \"timestamp\": 1485039219.614935\n" +
                "}";
        final RestResult result = mapper.readValue(data, RestResult.class);
        Assert.assertEquals(-0.5574, result.getSentiment(), 0.0);
        Assert.assertEquals("BBC do not even produce news anymore it is just left wing, anti Brexit, anti Trump pro EU pro immigration", result.getData());
        Assert.assertEquals(0, result.getTargets().size());
    }

    @Test
    public void testPayloadNoTargets() throws IOException {
        final String data = "{\n" +
                "  \"data\": \"BBC do not even produce news anymore it is just left wing, anti Brexit, anti Trump pro EU pro immigration\", \n" +
                "  \"sentiment\": -0.5574, \n" +
                "  \"timestamp\": 1485039219.614935\n" +
                "}";
        final RestResult result = mapper.readValue(data, RestResult.class);
        Assert.assertEquals(-0.5574, result.getSentiment(), 0.0);
        Assert.assertEquals("BBC do not even produce news anymore it is just left wing, anti Brexit, anti Trump pro EU pro immigration", result.getData());
        Assert.assertEquals(0, result.getTargets().size());
    }

    @Test
    public void testPayloadNoTargetsWithAnnotations() throws IOException {
        final String data = "{\n" +
                "  \"data\": \"BBC do not even produce news anymore it is just left wing, anti Brexit, anti Trump pro EU pro immigration\", \n" +
                "\"annotations\": [{\"id\":0,\"type\":\"Company\",\"start\":3,\"end\":0}],\n" +
                "  \"timestamp\": 1485039219.614935\n" +
                "}";
        final RestResult result = mapper.readValue(data, RestResult.class);
        Assert.assertEquals("BBC do not even produce news anymore it is just left wing, anti Brexit, anti Trump pro EU pro immigration", result.getData());
        Assert.assertEquals(0, result.getTargets().size());
        Assert.assertEquals(1, result.getAnnotations().size());
    }

    @Test
    public void testPayloadAspects() throws IOException {
        final String data = "{\n" +
                "  \"aspects\": {\n" +
                "    \" those piling into $amzn $nflx and $tsla today will probably be disappointed! the growth is already baked in the price.\": {\n" +
                "      \"aspect_name\": \"accounting\",\n" +
                "      \"aspect_text\": \"the growth\",\n" +
                "      \"score\": 0.3801170885562897\n" +
                "    },\n" +
                "    \"i may be wrong, \": {}\n" +
                "  },\n" +
                "  \"data\": \"I may be wrong, but those piling into $AMZN $NFLX and $TSLA today will probably be disappointed! The growth is already baked in the price.\",\n" +
                "  \"sentiment\": -0.0610344260931015,\n" +
                "  \"targets\": {\n" +
                "    \"amzn\": {\n" +
                "      \"sentiment\": -0.0610344260931015\n" +
                "    },\n" +
                "    \"nflx\": {\n" +
                "      \"sentiment\": -0.0610344260931015\n" +
                "    },\n" +
                "    \"tsla\": {\n" +
                "      \"sentiment\": -0.0610344260931015\n" +
                "    }\n" +
                "  },\n" +
                "  \"timestamp\": 1492693657\n" +
                "}";
        final RestResult result = mapper.readValue(data, RestResult.class);
        Assert.assertEquals(-0.0610, result.getSentiment(), 0.0001);
        Assert.assertEquals("I may be wrong, but those piling into $AMZN $NFLX and $TSLA today will probably be disappointed! The growth is already baked in the price.", result.getData());
        Assert.assertEquals(3, result.getTargets().size());
        Assert.assertEquals(2, result.getAspects().size());
    }

}
