package io.redlink.ssix.pipeline.nlp;

import io.redlink.ssix.pipeline.nlp.impl.RedlinkNamedEntityRecognizer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasItems;


/**
 * Integrations tests for the NER implementation relying on Redlink
 *
 * @author sergio.fernandez@redlink.co
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:context.xml")
public class RedlinkNamedEntityRecognizerIT {

    @Autowired
    private RedlinkNamedEntityRecognizer ner;

    @Test
    public void testEntities() {
        assertEntities("QQQ Is A Good Long Term Investment " +
                        "http://seekingalpha.com/article/3213016-qqq-is-a-good-long-term-investment?source=feed_f " +
                        "$AAPL #APPLE $AMZN $IBM $NFLX $NUAN $YHOO $QQQ",
                "aapl", "ibm", "amzn", "qqq");

        assertEntities("Apple retakes #1 slot as world’s most valuable brand $AAPL #aapl \n" +
                        "http://www.financialiceberg.com/apple_news_outlet.html",
                "aapl");

        assertEntities("$IBM going down",
                "ibm");

        assertEntities("$GOOG is doing really great",
                "goog");

        assertEntities("$GOOGL is doing really great",
                "googl");

    }

    private void assertEntities(String content, String... entities) {
        final Collection<String> extracted = ner.extract(content).stream()
                .map( e -> e.getEntity_name())
                .collect(Collectors.toList());
        Assert.assertThat(extracted, hasItems(entities));
    }

}

