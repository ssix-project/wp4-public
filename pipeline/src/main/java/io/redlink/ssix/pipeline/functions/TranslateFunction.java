package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.model.metadata.AnalyzerMetadata;
import io.redlink.ssix.pipeline.model.metadata.MachineTranslationMetadata;
import io.redlink.ssix.pipeline.nlp.api.Translator;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

import static io.redlink.ssix.pipeline.util.PipelineFunction.*;

public class TranslateFunction implements Function<Content,Content> {

    private static final Logger log = LoggerFactory.getLogger(TranslateFunction.class);

    public static final String TARGET_LANG = "en";

    private Translator translator;

    @Override
    public Content call(Content content) {

        final Map<String, AnalyzerMetadata> analyzers = content.getMetadata().getAnalyzers();

        if (!analyzers.containsKey(TRANSLATE.name()) ||
                (analyzers.containsKey(TRANSLATE.name()) && analyzers.get(TRANSLATE.name()).isEnabled())) {
            final AnalyzerMetadata metadata = new MachineTranslationMetadata()
                    .setName(translator.getClass().getCanonicalName());

            analyzers.put(TRANSLATE.name(), metadata);
        }

        if (analyzers.get(TRANSLATE.name()).isEnabled()) {
            final MachineTranslationMetadata metadata = (MachineTranslationMetadata) analyzers.get(TRANSLATE.name());
            metadata.setStart(new Date().getTime());
            metadata.setTargetLang(TARGET_LANG);
            metadata.setOriginalLang(content.getLanguage());
            if (TARGET_LANG.equals(content.getLanguage())) {
                metadata.setTranslated(false);
                log.debug("Content {} is already in {}, so it doesn't need to be translated", content.getUri(), content.getLanguage());
            } else {
                final String translation = translator.translate(content.getContent(), content.getLanguage(), TARGET_LANG);
                if (StringUtils.isNotBlank(translation)) {
                    log.debug("Replaced {} content by its translation from '{}'", content.getUri(), content.getLanguage());
                    log.trace("translated \"{}\"@{} to \"{}\"@{}", content.getContent(), content.getLanguage(), translation, TARGET_LANG);
                    metadata.setTranslated(true);
                    metadata.setOriginalContent(content.getContent());
                    content.setContent(translation);
                } else {
                    log.error("No translation for {}, from [{}] '{}'", content.getUri(), content.getLanguage(), content.getContent());
                }
            }
            metadata.setEnd(new Date().getTime());
        }

        return content;
    }

    public Translator getTranslator() {
        return translator;
    }

    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

}
