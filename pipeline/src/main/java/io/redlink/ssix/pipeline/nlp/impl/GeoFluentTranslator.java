package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.ssix.geofluent.GeoFluentClient;
import io.redlink.ssix.pipeline.nlp.api.Translator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.joining;

/**
 * GeoFluent native Java translator
 *
 * @author Sergio Fernández
 */
public class GeoFluentTranslator implements Translator {

    private static final Logger log = LoggerFactory.getLogger(GeoFluentTranslator.class);

    @Autowired
    private GeoFluentClient geoFluentClient;

    private Map<String, String> languages;

    public GeoFluentTranslator() {
        languages = new HashMap<>();
    }

    @Override
    public void init() {
        try {
            languages = geoFluentClient.languages();
            log.info("Initialized with languages: {}", languages
                                                            .entrySet()
                                                            .stream()
                                                            .map(entry -> entry.getKey() + "->"+ entry.getValue())
                                                            .collect(joining(", ")));
        } catch (IOException | URISyntaxException e) {
            log.error("Error retrieving supported languages: {}", e.getMessage());
            languages = new HashMap<>();
        }
    }

    @Override
    public void destroy() {
        languages = null;
    }

    @Override
    public String translate(String content, String source, String target) {
        final String normalizedSource = normalizeSourceLanguage(source);
        final String normalizedTarget = normalizeTargetLanguage(normalizedSource, target);
        try {
            return geoFluentClient.translate(content, normalizedSource, normalizedTarget);
        } catch (IOException | URISyntaxException e) {
            log.error("Error translating '{}' from {} to {}: {}", content, normalizedSource, normalizedTarget, e.getMessage());
            return ""; //FIXME: propagate exception
        }
    }

    private String normalizeSourceLanguage(String source) {
        return normalizeLanguage(source, languages.keySet());
    }

    private String normalizeTargetLanguage(String source, String target) {
        if (!languages.containsKey(source)) {
            return normalizeLanguage(target, languages.values());
        }
        return normalizeLanguage(target, Arrays.asList(languages.get(source)));
    }

    private String normalizeLanguage(String lang, Collection<String> supported) {
        if (supported.contains(lang)) {
            return lang;
        } else {
            for (String candidate: supported) {
                if (StringUtils.startsWith(candidate, lang)) {
                    log.debug("Language '{}' normalized to supported '{}'", lang, candidate);
                    return candidate;
                }
            }
            log.warn("Not target language for normalizing '{}'", lang);
            return lang;
        }
    }

    public void setGeoFluentClient(GeoFluentClient geoFluentClient) {
        this.geoFluentClient = geoFluentClient;
    }

}
