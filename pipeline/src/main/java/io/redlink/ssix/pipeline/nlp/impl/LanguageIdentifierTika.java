package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.ssix.pipeline.nlp.api.LanguageIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Language Identifier implemented on Tika
 *
 * @author Sergio Fernandez
 */
public class LanguageIdentifierTika implements LanguageIdentifier {

    private static final Logger log = LoggerFactory.getLogger(LanguageIdentifierTika.class);

    @Override
    public String identifyLanguage(String content) {
        final org.apache.tika.language.LanguageIdentifier identifier = new org.apache.tika.language.LanguageIdentifier(content);
        if (identifier != null) {
            return identifier.getLanguage();
        } else {
            log.error("no language identified for content: {}", content);
            return null;
        }
    }

}
