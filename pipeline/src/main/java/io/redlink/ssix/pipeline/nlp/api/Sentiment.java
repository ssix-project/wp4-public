package io.redlink.ssix.pipeline.nlp.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Sentiment v2, including also sentiment per targets
 *
 * @author sergio.fernandez@redlink.co
 */
public final class Sentiment {

    public static final int MIN_VALUE = -1000;
    public static final int MAX_VALUE = 1000;

    private static final Logger log = LoggerFactory.getLogger(Sentiment.class);

    private int value;
    private Map<String, Integer> targets;

    public Sentiment() {
        this(0);
    }

    public Sentiment(int value) {
        this.value = normalize(value);
        if (this.value != value) {
            log.warn("Invalid sentiment value '{}' has been normalized as '{}'", value, this.value);
        }
        this.targets = new HashMap<>();
    }

    public Sentiment(float value) {
        this((double) value);
    }

    public Sentiment(double value) {
        this((int)(value*1000));
    }

    public Sentiment(String value) {
        this(Integer.parseInt(value));
    }

    private int normalize(int value) {
        if (value < MIN_VALUE) {
            return MIN_VALUE;
        } else if (value > MAX_VALUE) {
            return MAX_VALUE;
        } else {
            return value;
        }
    }

    public int value() {
        return value;
    }

    public SentimentClass getClassName() {
        return SentimentClass.fromSentiment(this);
    }

    public String toString() {
        return String.format("%1$,.2f", value);
    }

    public Map<String, Integer> getTargets() {
        return targets;
    }

    public void setTargets(Map<String, Integer> targets) {
        this.targets = new HashMap<>(targets);
    }

    public void addTarget(String label, int sentiment) {
        this.targets.put(label, sentiment);
    }

    public void addTarget(String label, Sentiment sentiment) {
        this.addTarget(label, sentiment.value());
    }

}
