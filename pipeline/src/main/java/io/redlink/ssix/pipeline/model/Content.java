package io.redlink.ssix.pipeline.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.redlink.ssix.pipeline.model.metadata.Metadata;
import io.searchbox.annotations.JestId;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Content {

    @JestId
    private String id;

    private String uri;

    private String author;

    private String source;

    private String content;

    private long date;

    private String geo;

    private boolean shared;

    private String language;

    private String event;

    private int sentiment;

    private Collection<Target> targets;

    private Metadata metadata;

    public Content() {
        this.targets = new HashSet<>();
        this.metadata =  new Metadata();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
        if (StringUtils.isBlank(id)) {
            this.setId(uri.substring(uri.lastIndexOf("/") + 1));
        }
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getSentiment() {
        return sentiment;
    }

    public void setSentiment(int sentiment) {
        this.sentiment = sentiment;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public void setRetweet(boolean retweet) {
        this.setShared(retweet);
    }

    public Collection<Target> getTargets() {
        return targets;
    }

    public void setTargets(List<Target> targets) {
        this.targets = new HashSet(targets);
    }

    public void addTarget(String label, Integer sentiment) {
        final Target target = new Target();
        target.setEntity_name(label);
        target.setSentiment(sentiment);
        addTarget(target);
    }

    public void addTarget(String label) {
        final Target target = new Target();
        target.setEntity_name(label);
        addTarget(target);
    }

    public void addTarget(Target target) {
        if (targets.contains(target)){
            this.targets.remove(target);
        }
        this.targets.add(target);
    }

    public Target getTarget(String label) {
        return targets.stream().filter( t -> t.getEntity_name().equals(label)).findFirst().orElse(null);
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
