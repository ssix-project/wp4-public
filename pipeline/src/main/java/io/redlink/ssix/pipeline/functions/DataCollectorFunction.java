package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.DataCollector;
import io.redlink.ssix.pipeline.model.Content;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

public class DataCollectorFunction implements Consumer<Content> {

    private static final Logger log = LoggerFactory.getLogger(SentimentFunction.class);

    private DataCollector dataCollector;

    public void setDataCollector(DataCollector dataCollector) {
         this.dataCollector = dataCollector;
    }

    @Override
    public void accept(Content content) {
        dataCollector.put(content);
        log.debug("The content {} ḧas been send to the data collector", content.getUri());
    }

}
