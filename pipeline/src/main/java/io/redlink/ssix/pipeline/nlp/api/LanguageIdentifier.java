package io.redlink.ssix.pipeline.nlp.api;

import io.redlink.ssix.pipeline.model.Content;

import java.io.Serializable;

/**
 * Language Identifier
 *
 * @author Sergio Fernández
 */
public interface LanguageIdentifier extends Serializable  {

    default String identifyLanguage(Content content) {
        return identifyLanguage(content.getContent());
    }

    String identifyLanguage(String content);

}
