/*
 * Copyright (c) 2017 Redlink GmbH.
 */
package io.redlink.ssix.pipeline.model.metadata;

/**
 * Created on 21.12.17.
 */
public class MachineTranslationMetadata extends AnalyzerMetadata {

    private String targetLang;
    private String originalLang;
    private String originalContent;
    private boolean translated;

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }

    public String getOriginalLang() {
        return originalLang;
    }

    public void setOriginalLang(String originalLang) {
        this.originalLang = originalLang;
    }

    public String getOriginalContent() {
        return originalContent;
    }

    public void setOriginalContent(String originalContent) {
        this.originalContent = originalContent;
    }

    public boolean isTranslated() {
        return translated;
    }

    public void setTranslated(boolean translated) {
        this.translated = translated;
    }
}
