package io.redlink.ssix.pipeline.services;

import io.redlink.ssix.pipeline.functions.SentimentFunction;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;
import io.redlink.ssix.pipeline.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

/**
 * Simple configuration service
 *
 * @author Sergio Fernández
 */
@Service
public class ConfigurationService {

    private static final Logger log = LoggerFactory.getLogger(ConfigurationService.class);

    @Autowired
    private ApplicationContext context;

    public Map<String,Object> getConfigurations() {
        final Map<String, Object> conf = new HashMap<>();

        final SentimentFunction sentimentFunction = (SentimentFunction) context.getBean("sentimentFunction");
        final Class<? extends SentimentAnalyzer> domainSentimentAnalyzerClass = sentimentFunction.getDomainSentimentAnalyzer().getClass();
        final Class<? extends SentimentAnalyzer> genericSentimentAnalyzerClass = sentimentFunction.getGenericSentimentAnalyzer().getClass();
        final Map<String, Boolean> sentimentAnalyzers = new HashMap<>();
        for (SentimentAnalyzer analyzer: BeanFactoryUtils.beansOfTypeIncludingAncestors(context, SentimentAnalyzer.class).values()) {
            sentimentAnalyzers.put(analyzer.getClass().getCanonicalName(), domainSentimentAnalyzerClass.equals(analyzer.getClass()) || genericSentimentAnalyzerClass.equals(analyzer.getClass()));
        }
        conf.put("sentimentAnalyzers", sentimentAnalyzers);

        try {
            conf.putAll(SpringUtils.getSpringProperties(context));
        } catch (IOException e) {
            log.error("Error retrieving Spring properties: {}", e.getMessage(), e);
        }

        //TODO: what else?

        return conf;
    }

}
