package io.redlink.ssix.pipeline.nlp.impl;

import com.google.common.base.Optional;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObject;
import com.optimaize.langdetect.text.TextObjectFactory;
import io.redlink.ssix.pipeline.nlp.api.LanguageIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Language Identifier New Generation
 *
 * @author Sergio Fernández
 */
public class LanguageIdentifierNG implements LanguageIdentifier {

    private static final Logger log = LoggerFactory.getLogger(LanguageIdentifierNG.class);

    private transient LanguageDetector languageDetector;
    private transient TextObjectFactory textObjectFactory;

    public void init() throws IOException {
        init(true);
    }

    public void init(boolean shortTexts) throws IOException {
        final List<LanguageProfile> languageProfiles = new LanguageProfileReader().readAllBuiltIn();

        languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
                .withProfiles(languageProfiles)
                .build();

        textObjectFactory = shortTexts ?
                                CommonTextObjectFactories.forDetectingShortCleanText() :
                                CommonTextObjectFactories.forDetectingOnLargeText() ;

        log.info("Initialized LanguageIdentifierNG for detecting {} texts", shortTexts ? "short" : "large");
    }

    @Override
    public String identifyLanguage(String content) {
        //FIXME
        if (languageDetector == null || textObjectFactory == null) {
            try {
                init();
            } catch (IOException e) {
                log.error("Error lazy initializing LanguageIdentifierNG: {}", e.getMessage(), e);
            }
        }

        final String cleanupContent = content.replaceAll("http.*?\\s", " ");
        Optional<LdLocale> lang = languageDetector.detect(textObjectFactory.forText(cleanupContent));
        if(lang.isPresent()) {
            return lang.get().getLanguage();
        } else {
            Optional<LdLocale> lang2 = languageDetector.detect(textObjectFactory.forText(content));
            return lang2.isPresent() ? lang2.get().getLanguage() : null;
        }
    }

}
