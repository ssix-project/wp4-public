package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.sdk.RedLink;
import io.redlink.sdk.RedLinkFactory;
import io.redlink.sdk.impl.analysis.AnalysisRequest;
import io.redlink.sdk.impl.analysis.model.Enhancements;
import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.nlp.api.External;
import io.redlink.ssix.pipeline.nlp.api.Sentiment;
import io.redlink.ssix.pipeline.nlp.api.SentimentAnalyzer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Sentiment analyzer implementation relying on the Redlkin Platform
 *
 * @see <a href="http://dev.redlink.io/api">Redlink API</a>
 * @author sergio.fernandez@redlink.co
 */
public class RedlinkSentimentAnalyzer implements External, SentimentAnalyzer {

    private static final Logger log = LoggerFactory.getLogger(RedlinkSentimentAnalyzer.class);

    private String apiKey;
    private RedLink.Analysis redlink;

    protected boolean available;

    @Override
    public void init() {
        redlink = RedLinkFactory.createAnalysisClient(apiKey);
        available = true;
        log.info("initialized redlink client with key '{}'", apiKey);
    }

    @Override
    public Sentiment analyze(Content content) {
        if (Objects.isNull(content) || StringUtils.isBlank(content.getContent())) {
            log.warn("empty/null content, so actually we skip the call");
            return new Sentiment();
        } else {
            final AnalysisRequest request = AnalysisRequest.builder()
                    .setContent(content.getContent())
                    .setOutputFormat(AnalysisRequest.OutputFormat.TURTLE).build();
            final Enhancements enhancements = redlink.enhance(request);
            final Double sentiment = enhancements.getDocumentSentiment();
            return new Sentiment(sentiment);
        }
    }

    @Override
    public void destroy() {
        redlink = null;
        available = false;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public boolean isAvailable() {
        return available;
    }

    @Override
    public void setAvailable(boolean available) {
        this.available = available;
    }

}
