package io.redlink.ssix.pipeline.nlp.impl;

import io.redlink.ssix.pipeline.nlp.api.LanguageIdentifier;
import org.insight_centre.kdu.textcat.TextCategorizer;

/**
 * Language Identifier based on TextCat
 *
 * @author Gopal KS
 */
public class LanguageIdentifierTextCat implements LanguageIdentifier {

    private TextCategorizer textcat;

    public void init() {
        textcat = new TextCategorizer();
    }

    /**
     * categorizes the text passed to it
     *
     * @param text
     *            text to be categorized
     * @return the category name given in the configuration file
     */
    public String identifyLanguage(String text) {
        final String preprocessedText = text
                .toLowerCase()
                //.replaceAll("http.*?\\s", " ")
                .replaceAll("\\.|/|:", " ");
        return textcat.categorize(preprocessedText);
    }

}

