package io.redlink.ssix.pipeline.functions;

import io.redlink.ssix.pipeline.model.Content;
import io.redlink.ssix.pipeline.model.metadata.AnalyzerMetadata;
import io.redlink.ssix.pipeline.nlp.api.LanguageIdentifier;
import io.redlink.ssix.pipeline.util.PipelineFunction;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

import static io.redlink.ssix.pipeline.util.PipelineFunction.*;

public class LanguageIdentificationFunction implements Function<Content,Content> {

    private static final Logger log = LoggerFactory.getLogger(LanguageIdentificationFunction.class);

    private LanguageIdentifier languageIdentifier;

    @Override
    public Content call(Content content) {

        final Map<String, AnalyzerMetadata> analyzers = content.getMetadata().getAnalyzers();

        if (!analyzers.containsKey(LANG_IDENT.name()) ||
                (analyzers.containsKey(LANG_IDENT.name()) && analyzers.get(LANG_IDENT.name()).isEnabled())) {
            final AnalyzerMetadata metadata = new AnalyzerMetadata()
                    .setName(languageIdentifier.getClass().getCanonicalName());

            analyzers.put(LANG_IDENT.name(), metadata);
        }

        final AnalyzerMetadata metadata = analyzers.get(LANG_IDENT.name());
        if (metadata.isEnabled()) {
            metadata.setStart(new Date().getTime());
            if (StringUtils.isNotBlank(content.getContent())) {
                final String language = languageIdentifier.identifyLanguage(content);
                if (StringUtils.isNotBlank(language)) {
                    content.setLanguage(language);
                    log.debug("set language for content {} as {}", content.getUri(), content.getLanguage());
                } else {
                    log.warn("no language identified for content {}", content.getUri());
                }
            } else {
                log.warn("skipped language identification for blank content");
            }
            metadata.setEnd(new Date().getTime());
        }
        return content;
    }

    public void setLanguageIdentifier(LanguageIdentifier languageIdentifier) {
        this.languageIdentifier = languageIdentifier;
    }

}
