package io.redlink.ssix.pipeline.model;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Target entity
 *
 * @author Sergio Fernández
 */
public class Target {

    private double sentiment;

    private String  entity_name;

    private String entity_id;

    private String parent_id;

    private String  entity_type;

    private String domain;

    private String uri;

    private Collection<Annotation> annotations = new ArrayList<>();

    //private List<Aspect> aspects;


    public Target() {
    }

    public double getSentiment() {
        return sentiment;
    }

    public Target setSentiment(double sentiment) {
        this.sentiment = sentiment;
        return this;
    }

    public String getEntity_name() {
        return entity_name;
    }

    public Target setEntity_name(String entity_name) {
        this.entity_name = entity_name;
        return this;
    }

    public String getEntity_id() {
        return entity_id;
    }

    public Target setEntity_id(String entity_id) {
        this.entity_id = entity_id;
        return this;
    }

    public Target setParent_id(String parent_id) {
        this.parent_id = parent_id;
        return this;
    }

    public String getParent_id() {
        return parent_id;
    }

    public Target setEntity_type(String entity_type) {
        this.entity_type = entity_type;
        return this;
    }

    public String getDomain() {
        return domain;
    }

    public Target setDomain(String domain_topic) {
        this.domain = domain_topic;
        return this;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public String getUri() {
        return uri;
    }

    public Target setUri(String uri) {
        this.uri = uri;
        return this;
    }

    public Collection<Annotation> getAnnotations() {
        return annotations;
    }

    public Target addAnnotation(Annotation annotation) {
        this.annotations.add(annotation);
        return this;
    }

    public Target addAnnotation(Collection<Annotation> annotation) {
        this.annotations.addAll(annotation);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Target target = (Target) o;

        //TODO: add an equals based on annotation position
        return entity_name != null ? entity_name.equals(target.entity_name) : false;

    }

    @Override
    public int hashCode() {
        return entity_name != null ? entity_name.hashCode() : 0;
    }

    public static String uniqueFunction(Target t) {
        if (t.entity_type.toLowerCase().equals("stock") && StringUtils.isNotEmpty(t.parent_id)) {
            return t.parent_id;
        }
        if(StringUtils.isNotEmpty(t.entity_id)) {
            return t.entity_id;
        }
        return t.entity_id;
    }
}
