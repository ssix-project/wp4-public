package io.redlink.ssix.pipeline.nlp.api;

import io.redlink.ssix.pipeline.model.Target;

import java.io.Serializable;
import java.util.Collection;

/**
 * Named-EntityRecognizer interface
 *
 * @author Sergio Fernández
 */
public interface NamedEntityRecognizer extends Serializable {

    void init();

    Collection<Target> extract(String content);

    void destroy();

}
