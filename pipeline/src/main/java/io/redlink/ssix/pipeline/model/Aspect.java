package io.redlink.ssix.pipeline.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Aspect bean
 *
 * @author Sergio Fernández
 */
public class Aspect {

    @JsonProperty("aspect_name")
    private String name;

    @JsonProperty("aspect_text")
    private String text;

    private double score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

}
