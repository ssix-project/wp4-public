package io.redlink.ssix.pipeline.nlp.api;

import io.redlink.ssix.pipeline.model.Prediction;

import java.io.Serializable;

/**
 * Event Recognizer interface
 *
 * @author Gopal
 * @author Alanna
 */
public interface EventRecognizer extends Serializable {

    Prediction predict(String content);

}