package io.redlink.ssix.pipeline.services;

import io.redlink.ssix.pipeline.nlp.api.SentimentClass;
import io.searchbox.client.JestClient;
import io.searchbox.core.Count;
import io.searchbox.core.CountResult;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.HistogramAggregation;
import io.searchbox.core.search.aggregation.Range;
import io.searchbox.core.search.aggregation.RangeAggregation;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Simple analytics service
 *
 * @author Sergio Fernández
 */
@Service
public class AnalyticsService {

    private static final Logger log = LoggerFactory.getLogger(AnalyticsService.class);

    @Autowired
    private JestClient esClient;

    public int getSentimentsCount() throws IOException {
        final Count count = new Count.Builder()
                .addIndex("content")
                .build();
        final CountResult result = esClient.execute(count);
        return parseSentimentsCount(result);
    }

    public Map<SentimentClass, Integer> getSentimentsAggregation() throws IOException {
        final String query = IOUtils.toString(this.getClass().getClassLoader()
                .getResourceAsStream("queries/sentiment_aggregation.json"));
        final Search search = new Search.Builder(query)
                .addIndex("content")
                .build();
        final SearchResult result = esClient.execute(search);
        return parseSentimentsAggregation(result);
    }

    public List<Pair<Integer,Double>> getAverageSentimentSeries() throws IOException {
        final String query = IOUtils.toString(this.getClass().getClassLoader()
                .getResourceAsStream("queries/sentiment_series.json"));
        final Search search = new Search.Builder(query)
                .addIndex("content")
                .build();
        final SearchResult result = esClient.execute(search);
        return parseSentimentsSeries(result);
    }

    protected static Map<SentimentClass, Integer> parseSentimentsAggregation(SearchResult result) {
        final Map<SentimentClass, Integer> distribution = getEmptyDistribution();
        log.trace("Raw result JSON: \n{}", result.getJsonString());
        final RangeAggregation aggregation = result.getAggregations()
                .getRangeAggregation("group_by_sentiment");
        if (aggregation == null) {
            log.error("No aggregation found!");
        } else {
            for (Range range: aggregation.getBuckets()) {
                distribution.put(SentimentClass.fromValue((range.getFrom() + range.getTo()) / 2),
                                    range.getCount().intValue());
            }
        }
        return distribution;
    }

    protected static List<Pair<Integer,Double>> parseSentimentsSeries(SearchResult result) {
        log.trace("Raw result JSON: \n{}", result.getJsonString());
        final HistogramAggregation aggregation = result.getAggregations()
                .getHistogramAggregation("sentiment_histogram");
        if (aggregation == null) {
            log.error("No aggregation found!");
            return Collections.emptyList();
        } else {
            final List<Pair<Integer,Double>> series = new ArrayList<>();
            for (final HistogramAggregation.Histogram bucket: aggregation.getBuckets()) {
                final ImmutablePair<Integer,Double> tuple = new ImmutablePair<>(
                                                                bucket.getCount().intValue(),
                                                                bucket.getAvgAggregation("average").getAvg());
                series.add(tuple);
            }
            return series;
        }
    }

    protected static int parseSentimentsCount(CountResult result) {
        log.trace("Raw result JSON: \n{}", result.getJsonString());
        if (result.getValue("count") != null) {
            return ((Double) result.getValue("count")).intValue();
        } else {
            return 0;
        }
    }

    protected static Map<SentimentClass, Integer> getEmptyDistribution() {
        return Arrays.asList(SentimentClass.values())
                .stream()
                .filter(c -> !SentimentClass.Irrelevant.equals(c))
                .sorted()
                .collect(Collectors.toMap((c)->c, (c)->0));
    }

}
