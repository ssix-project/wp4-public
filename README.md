# SSIX Analysis Pipeline

This Anlysis Pipeline consists on the following pieces:

* Social data is read from a [Kafka](http://kafka.apache.org/) queue
    * Aa simple Twitter collector has been implemented in Python for feeding the queue from the [Twitter Streaming API](http://dev.twitter.com/pages/streaming_api_methods)
* Data is process using [Spark](https://spark.apache.org/)
* Different NLP components are provided implementing the four NLP tasks (language identification, machine translation, named-entity recognition, sentiment analysis)
* Results are cached into [ElasticSearch](https://www.elastic.co/)

## Get it running

There are three methods for running this version (we used to have four):

1. [Manual](#markdown-header-manual)
2. [Orchestrated containers](#markdown-header-containers)
3. [DC/OS](#markdown-header-dcos)

Where the last one is preferred, and actually the one implemented anticipating the integration with tto provide an integrated demo of the SSIX Platform.

### Manual

Manually running this prototype could be a bit complex, and it's expected that __only developers__ draw on this options. 
If you just want to run it, consider the rest of options described below.

To use this option you need to have a JRE 7.x (Java Runtime Environment) installed, and at least 4GB of RAM (so then export 
that setting by doing `export MAVEN_OPTS="$MAVEN_OPTS -Xmx4g"`). The commands described are only suitable for unix systems
and can be incompatible with other systems; consider the containers alternatives if you're using Windows.

Having said that, you have to follow these steps:

1. Install some required Python libraries: 
    * `sudo pip install -r collector/requirements.txt --upgrade`
    * `sudo pip install -r conf/requirements.txt --upgrade`
2. [Download Kafka](http://kafka.apache.org/downloads.html): `wget http://www.eu.apache.org/dist/kafka/0.8.2.2/kafka_2.11-0.8.2.2.tgz -O kafka_2.11-0.8.2.2.tgz`
3. Uncompress Kafka: `tar -zxf kafka_2.11-0.8.2.2.tgz`
4. Start Zookeper: `kafka_2.11-0.8.2.2/bin/zookeeper-server-start.sh kafka_2.11-0.8.2.2/config/zookeeper.properties`
5. Copy the required configuration to Zookeeper: `python conf/configuration.py`
6. Start Kafka (see the [quickstart guide](http://kafka.apache.org/documentation.html#quickstart) for the details): `kafka_2.11-0.8.2.2/bin/kafka-server-start.sh kafka_2.11-0.8.2.2/config/server.properties`
7. Create a topic (queue): `kafka_2.11-0.8.2.2/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic content`
8. Download ElasticSearch: `wget https://download.elasticsearch.org/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/2.3.0/elasticsearch-2.3.0.tar.gz`
9. Uncompress ElasticSearch: `tar -zxvf elasticsearch-2.3.0.tar.gz`
10. Copy the configuration: `cp elasticsearch/elasticsearch.yml elasticsearch-2.3.0/config/elasticsearch.yml`
11. Start ElasticSearch: `./elasticsearch-2.3.0/bin/elasticsearch`
12. Start the GATE based NER service: see [README](components/ner_service/README.md)
13. Install the classifier: see [README](components/financial_classifier/README.md)
14. Start the classifier: `python components/financial_classifier/service.py`
15. Launch the simple python collector: `python collector/twitter.py`
16. Start Spark: http://spark.apache.org/docs/1.6.3/#running-the-examples-and-shell ([spark-1.6.3-bin-hadoop2.6.tgz](http://archive.apache.org/dist/spark/spark-1.6.3/spark-1.6.3-bin-hadoop2.6.tgz))
17. Build the Pipeline: `mvn -f pipeline/pom.xml package -DskipTests`
18. [Submit](https://spark.apache.org/docs/1.6.3/submitting-applications.html) the Pipeline:

```
./bin/spark-submit \
  --class io.redlink.ssix.pipeline.SSIXAnalysisSparkPipeline \
  --master local \
  ../pipeline/target/ssix-pipeline-3.0.0-SNAPSHOT.jar
```

Optionally, any setting can be customize at submission time by adding arguments. For instance, if the classifier is running on a different port, 
appending `-Dfinancial.port=5001` to the previous command-line to ponit to the custom port. 

### Containers

A [configuration](docker-compose.yml) using [Docker Compose](https://docs.docker.com/compose/) is provided 
for [linking](https://docs.docker.com/compose/compose-file/#links) all required containers. 

Then you just need to execute the following commands:

1. Build all services: `docker-compose build`
2. Create and start all containers: `docker-compose up`
    * `Ctrl+C` will gracefully stop all containers,
    * or use the `-d` option run them detached (i.e., in the background)
3. You can access the Spark UI at [localhost:8080](http://localhost:8080/),
4. Submit the Pipeline:
```
./bin/spark-submit \
  --class io.redlink.ssix.pipeline.SSIXAnalysisSparkPipeline \
  --master spark://localhost:7077 \
  ../pipeline/target/ssix-pipeline-3.0.0-SNAPSHOT.jar
```
Once you have stopped the container, you can clean-up your local repository: `docker-compose rm && docker rmi $(docker images -q wp4*)`

### DC/OS

On DC/OS all the previous infrastrcuture is already managed, so you just need to [submit the Pipeline as a regular Spark job](https://docs.mesosphere.com/1.8/usage/service-guides/spark/run-job/):
```
dcos spark run --submit-args="--class io.redlink.ssix.pipeline.SSIXAnalysisSparkPipeline ssix-pipeline-3.0.0-SNAPSHOT.jar \
-Dgate.ner.host=ssix-gate-ner-service.marathon.l4lb.thisdcos.directory \
-Dgate.ner.port=8085 \
-Dfinancial.host=ssix-financial-classifier.marathon.l4lb.thisdcos.directory \
-Delasticsearch.host=ssix-elasticsearch.marathon.l4lb.thisdcos.directory \
-Dzookeeper.quorum=master.mesos:2181/dcos-service-kafka"
```
